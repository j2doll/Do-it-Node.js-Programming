/**
 * 3장 Test 10
 * 
 * 배열 안의 모든 요소를 하나씩 확인하기
 */

var Users = [
	{name:'소녀시대',age:20},
	{name:'걸스데이',age:22},
	{name:'티아라',age:23}
];

console.log('배열 요소의 수 : %d', Users.length);
for (var i = 0; i < Users.length; i++) {
	console.log('배열 요소 #' + i + ' : %s', Users[i].name);
} 

console.log('\nforEach 구문 사용하기');
Users.forEach( function(item, index) {
	console.log('배열 요소 #' + index + ' : %s', item.name);
} );
// [배열].foreach( [콜백함수] )
 // 콜백함수 구문 : function callbackfn(value, index, array1)
 // value : 배열 요소의 값
 // index : 배열 요소의 숫자 인덱스
 // array1 : 요소가 포함된 배열 개체

 